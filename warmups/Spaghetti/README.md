## 2021 Hacktivity CFT Warmup -- Spaghettil

Downloaded PNG file, looked at image, nothing special. 

Get metadata to look for hints 

```bash
exiftool Spaghetti.png

ExifTool Version Number         : 12.30
File Name                       : spaghetti.png
Directory                       : .
File Size                       : 194 KiB
File Modification Date/Time     : 2021:09:14 14:02:29-07:00
File Access Date/Time           : 2021:09:14 15:53:19-07:00
File Inode Change Date/Time     : 2021:09:14 14:03:01-07:00
File Permissions                : -rw-r--r--
File Type                       : PNG
File Type Extension             : png
MIME Type                       : image/png
Image Width                     : 520
Image Height                    : 300
Bit Depth                       : 8
Color Type                      : RGB with Alpha
Compression                     : Deflate/Inflate
Filter                          : Adaptive
Interlace                       : Noninterlaced
Background Color                : 255 255 255
Warning                         : [minor] Text/EXIF chunk(s) found after PNG IDAT (may be ignored by some readers)
Software                        : Adobe ImageReady
Image Size                      : 520x300
Megapixels                      : 0.156
```

This gives us one hint in the *Warning* data and we have the *bit depth*. Let's see what we can get with hexdump now

```bash
hexdump -C -n 8 spaghetti.png

00000000  89 50 4e 47 0d 0a 1a 0a                           |.PNG....|
00000008
```

Cool, we get some chunck in formation. Let's do a full dump and see what we get from there (going to put it in a file because it's big).

```bash
hexdump -C spaghetti.png
```
[output](https://github.com/rdkohalmy/Hacktivity-CTF-2021/blob/main/warmups/Spaghetti/hexdump.txt)


That hexdump is a lot to comb over. I tried `strings` but that got me nothing. From other crypto/stego from John that he really likes to hide stuff in a odd way. There a ton of periods in this so I ditched them. Now I just did a find for flag in the [hexdump-remove-dots](https://github.com/rdkohalmy/Hacktivity-CTF-2021/blob/main/warmups/Spaghetti/hexdump-remove-dots.txt) and found this. 

```bash
|flag{w|
|ow_that_|
|was_a_lo|
|ng_strin|
|g_of_spa|
|ghetti}|
```

Now we have the flag and can get our points. `flag{wow_that_was_a_long_string_of_spaghetti}`


Cryptography and Stegography are fun CTF games and I definatley learned something new when doing reasearch on PNG files. Here are a few that I thought were interesting an may help in future Stego challenges.  


A common steganography trick is to hide a message in the least significant bits (LSB) of an image. Raster images like PNG images are made up of pixels.

Placing shells in IDAT chunks has some big advantages and should bypass most data validation techniques where applications resize or re-encode uploaded images. 
